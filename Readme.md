# Satisfactory container image

This is a container image for the [Satisfactory](https://www.satisfactorygame.com/) server. Loosely based
on [wolveix/satisfactory-server](https://github.com/wolveix/satisfactory-server).

## Usage

You can use the included [docker-compose.yml](docker-compose.yml). The following environment variables are currently
supported:

| Variable                | Default   | Description                            |
|-------------------------|-----------|----------------------------------------|
| `AUTOPAUSE`             | `true`    | Game option                            |
| `AUTOSAVEINTERVAL`      | `300`     | Game option                            |
| `AUTOSAVENUM`           | `3`       | Game option                            |
| `AUTOSAVEONDISCONNECT`  | `true`    | Game option                            |
| `CRASHREPORT`           | `true`    | Game option                            |
| `DISABLESEASONALEVENTS` | `false`   | Game option                            |
| `MAXPLAYERS`            | `4`       | Game option                            |
| `SERVERIP`              | `0.0.0.0` | Game option                            |
| `SERVERBEACONPORT`      | `15000`   | Game option                            |
| `SERVERGAMEPORT`        | `7777`    | Game option                            |
| `SERVERQUERYPORT`       | `15777`   | Game option                            |
| `SKIPUPDATE`            | `false`   | Don't update after the initial install |
| `BETABRANCH`            | `public`  | Branch of server on Steam              |
| `STEAMAPPID`            | `1690800` |                                        |
| `ALWAYSVALIDATE`        | `false`   | Validate game files every time         |


The image is using two volumes:

- `/server` - Server files
- `/saves`- Game saves