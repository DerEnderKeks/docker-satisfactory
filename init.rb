#!/usr/bin/env ruby
# frozen_string_literal: true

require 'fileutils'
require 'inifile'

# file -> section -> key -> value
config_files = {
  'Engine.ini' => {
    '/Script/FactoryGame.FGSaveSession' => {
      mNumRotatingAutosaves: ENV.fetch('AUTOSAVENUM', '3')
    },
    'CrashReportClient' => {
      bImplicitSend: ENV.fetch('CRASHREPORT', 'true')
    },
    '/Script/OnlineSubsystemUtils.IpNetDriver' => {
      NetServerMaxTickRate: 120,
      MaxNetTickRate: 400,
      MaxInternetClientRate: 104_857_600,
      MaxClientRate: 104_857_600,
      LanServerMaxTickRate: 120,
      InitialConnectTimeout: 300.0,
      ConnectionTimeout: 300.0
    },
    '/Script/SocketSubsystemEpic.EpicNetDriver' => {
      MaxClientRate: 104_857_600,
      MaxInternetClientRate: 104_857_600
    },
    '/Script/EngineSettings.GameMapsSettings' => {
      GameDefaultMap: '/Game/FactoryGame/Map/GameLevel01/Persistent_Level',
      LocalMapOptions: '?sessionName=SatisfactoryServer?Visibility=SV_FriendsOnly?loadgame=savefile?listen?bUseIpSockets?name=Host'
    }
  },
  'Game.ini' => {
    '/Script/Engine.GameSession' => {
      MaxPlayers: ENV.fetch('MAXPLAYERS', '4')
    },
    '/Script/Engine.GameNetworkManager' => {
      TotalNetBandwidth: 104857600,
      MaxDynamicBandwidth: 104857600,
      MinDynamicBandwidth: 10485760
    }
  },
  'GameUserSettings.ini' => {
    '/Script/FactoryGame.FGGameUserSettings' => {
      mFloatValues: "((\"FG.AutosaveInterval\", #{ENV.fetch('AUTOSAVEINTERVAL', '300')}))",
      mIntValues: "((\"FG.DisableSeasonalEvents\", #{ENV.fetch('DISABLESEASONALEVENTS', 'false') == 'true' ? 1 : 0}))"
    }
  },
  'Scalability.ini' => {
    'NetworkQuality@3' => {
      TotalNetBandwidth: 104_857_600,
      MaxDynamicBandwidth: 104_857_600,
      MinDynamicBandwidth: 10_485_760
    }
  },
  'ServerSettings.ini' => {
    '/Script/FactoryGame.FGServerSubsystem' => {
      mAutoPause: ENV.fetch('AUTOPAUSE', 'true'),
      mAutoSaveOnDisconnect: ENV.fetch('AUTOSAVEONDISCONNECT', 'true')
    }
  }
}

puts 'Patching config files...'
config_base_path = '/server/FactoryGame/Saved/Config/LinuxServer'
FileUtils.mkdir_p config_base_path
config_files.each do |file_name, sections|
  config_file_path = File.join(config_base_path, file_name)
  FileUtils.touch config_file_path # files don't exist before running the server once
  config_file = IniFile.load config_file_path

  sections.each do |section, keys|
    keys.each do |key, value|
      config_file[section][key.to_s] = value
    end
  end

  config_file.write
end

server_missing = !File.exist?('/server/Engine/Binaries/Linux/UE4Server-Linux-Shipping')
skip_update = ENV['SKIPUPDATE'] == 'true'

if !skip_update || server_missing
  puts 'Updating server...'

  app_id = ENV.fetch 'STEAMAPPID'
  beta_flag = "-beta #{ENV.fetch 'BETABRANCH', 'public'}"
  validate_flag = server_missing || ENV['ALWAYSVALIDATE'] == 'true' ? 'validate ' : ''

  update_cmd = 'steamcmd.sh +force_install_dir /server +login anonymous '\
               "+app_update \"#{app_id} #{beta_flag}\" #{validate_flag}+quit"
  puts "Executing: '#{update_cmd}'"
  update_success = system update_cmd

  unless update_success
    puts 'Update failed.'
    exit 1
  end
else
  puts 'Skipping update.'
end

puts 'Linking saves directory...'
FileUtils.mkdir_p '/home/steam/.config'
FileUtils.ln_s '/saves', '/home/steam/.config/Epic', force: true

puts 'Starting server...'
server_cmd = '/server/Engine/Binaries/Linux/UE4Server-Linux-Shipping'
server_args = 'FactoryGame -log -NoSteamClient -unattended '\
             "?listen -Port=#{ENV.fetch 'SERVERGAMEPORT'} -BeaconPort=#{ENV.fetch 'SERVERBEACONPORT'} "\
             "-ServerQueryPort=#{ENV.fetch 'SERVERQUERYPORT'} -multihome=#{ENV.fetch 'SERVERIP'}"

puts "Executing: '#{server_cmd} #{server_args}'"
system 'true' # for some reason nothing is printed between the last system() call and exec()
exec server_cmd, *server_args.split(' ')
