FROM derenderkeks/rocky-steamcmd
LABEL maintainer="DerEnderKeks"

USER root
RUN set -x && \
    dnf install -y ruby && \
    dnf clean all && \
    gem install inifile

RUN install -d -m 0755 -o $USER -g $USER /server /saves

COPY init.rb /
USER $USER

ENV AUTOPAUSE="true" \
    AUTOSAVEINTERVAL="300" \
    AUTOSAVENUM="3" \
    AUTOSAVEONDISCONNECT="true" \
    CRASHREPORT="true" \
    DISABLESEASONALEVENTS="false" \
    MAXPLAYERS="4" \
    SERVERIP="0.0.0.0" \
    SERVERBEACONPORT="15000" \
    SERVERGAMEPORT="7777" \
    SERVERQUERYPORT="15777" \
    SKIPUPDATE="false" \
    BETABRANCH="public" \
    STEAMAPPID="1690800" \
    ALWAYSVALIDATE="false"

VOLUME /saves
VOLUME /server

EXPOSE 7777/udp 15000/udp 15777/udp

ENTRYPOINT ["/init.rb"]